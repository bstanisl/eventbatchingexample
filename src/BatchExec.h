#ifndef BATCHEXEC_H_
#define BATCHEXEC_H_

#include <array>
#include <atomic>
#include <chrono>
#include <memory>
#include <semaphore>
#include <thread>
#include <type_traits>

#include <fmt/chrono.h>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <tbb/queuing_rw_mutex.h>
#include <tbb/spin_rw_mutex.h>

#include <boost/asio/io_context.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/callable_traits.hpp>

using namespace std::chrono_literals;

template <int N, class InputData, class OutputData, class F, class Enabled = void> class BatchExec_;
template <int N, class InputData, class OutputData, class F>
class BatchExec_<N, InputData, OutputData, F,
                 typename std::enable_if_t<std::is_invocable_v<
                       F, int, const std::array<InputData, N>&, std::array<OutputData, N>&>>> {
  public:
    class Slot {
      public:
        Slot(int slot, BatchExec_* parent)
            : input(&(parent->input->at(slot))), slot(slot), parent(parent) {}
        Slot() = delete;
        Slot(const Slot&) = delete;

        ~Slot() {
            // Give up "read" lock on output if we're holding one
            if (output) {
                output_lock.release();
            }
        }
        InputData* input;
        OutputData* output = nullptr;
        int slot;
        void wait_for_output() {
            // return the "read" lock
            parent->input_mutex.unlock();
            if (slot % N == N - 1 || !parent->initialized) {
                // batch is filled -- expire the timer
                // !parent->initialized ensures we run immediately on the first event
                parent->timer.cancel();
            }
            // set output ptr
            output = &(parent->output->at(slot));
            // wait for worker
            // Get a "read" lock on the output
            output_lock.acquire(parent->output_mutex, false);
        }

      private:
        BatchExec_* parent;
        tbb::queuing_rw_mutex::scoped_lock output_lock{};
    };
    BatchExec_(F&& worker)
        : worker(std::move(worker)),
          semaphore{N},
          slot{0},
          input_mutex{},
          input{std::make_unique<std::array<InputData, N>>()},
          output{std::make_unique<std::array<OutputData, N>>()},
          ctx(),
          timer(ctx),
          submit_thread(&BatchExec_::submit_loop, this) {
        // try sleeping to give the submit thread time to start
        std::this_thread::sleep_for(10ms);
    }
    BatchExec_(F& worker) : BatchExec_(std::move(worker)) {}
    BatchExec_(const BatchExec_&) = delete;
    ~BatchExec_() {
        timer.cancel();
        submit_thread.join();
    }

    Slot get_slot() {
        // wait for a free slot
        semaphore.acquire();
        // Get slot number
        int slot_num = slot.fetch_add(1);
        // Grab a "read" lock so worker doesn't run while we're still writing
        input_mutex.lock_read();
        // Return slot
        return Slot(slot_num, this);
    }

  private:
    // CONFIGURABLE:
    static constexpr auto timeout = 5s;

    F&& worker;
    std::counting_semaphore<N> semaphore; // Only N events can be processed at once
    std::atomic<int> slot;                // Provide slot numbers
    tbb::spin_rw_mutex input_mutex;       // Prevent writes while offloaded processing occurs
    tbb::queuing_rw_mutex output_mutex; // Prevent starting the next round of processing before the
                                        // output from the previous has been read
    std::unique_ptr<std::array<InputData, N>> input;   // Hold input data
    std::unique_ptr<std::array<OutputData, N>> output; // Hold output data
    boost::asio::io_context ctx; // IO context. Don't actually use this since we aren't using async
                                 // waits.
    boost::asio::steady_timer timer; // Run worker every timeout even if slots aren't full
    std::thread submit_thread; // Thread to run worker whenever slots are full or timer expires
    tbb::queuing_rw_mutex::scoped_lock output_lock{};
    bool initialized = false;

    void async_submit(const boost::system::error_code&) {
        // get "write" lock to make sure nothing is writing input data
        tbb::spin_rw_mutex::scoped_lock input_lock(input_mutex);
        // determine number of filled slots (and reset atomic)
        int filled_slots = slot.exchange(0);
        // do work if any slots were filled, else we're done
        if (filled_slots) {
            initialized = true;
            auto start = std::chrono::steady_clock::now();
            std::invoke(worker, filled_slots, *input, *output);
            fmt::print("Now = {:%H:%M:%S}, Worker took {} ns. Filled = {}\n",
                       std::chrono::system_clock::now(),
                       (std::chrono::steady_clock::now() - start) / 1ns, filled_slots);
        }
        else if (initialized) {
            output_lock.release();
            return;
        }
        (*input) = {};
        // tell threads waiting for output that they can read
        output_lock.release();
        // wait for threads to finish reading
        output_lock.acquire(output_mutex);
        // reset timer and release next batch of input slots
        timer.expires_after(timeout);
        semaphore.release(filled_slots);
        // schedule new async wait
        timer.async_wait(std::bind_front(&BatchExec_::async_submit, this));
    }

    void submit_loop() {
        // start with output locked and timer set to timeout
        output_lock.acquire(output_mutex);
        timer.expires_after(
              5min); // Set initial timeout to 5 min in case initialization takes a long time
        // wait for timer to expire (occurs when slots are full or after timeout)
        timer.async_wait(std::bind_front(&BatchExec_::async_submit, this));
        ctx.run();
    }
};

template <class F> class BatchExec_worker {
    // Small traits utility for BatchExec definition
  private:
    using args = boost::callable_traits::args_t<F>;
    using input_array_t = std::remove_cvref_t<std::tuple_element_t<1, args>>;
    using output_array_t = std::remove_cvref_t<std::tuple_element_t<2, args>>;

  public:
    using input_t = std::tuple_element_t<0, input_array_t>;
    using output_t = std::tuple_element_t<0, output_array_t>;
    static constexpr int N = std::tuple_size_v<input_array_t>;
};

template <class F>
BatchExec_(F&&) -> BatchExec_<BatchExec_worker<F>::N, typename BatchExec_worker<F>::input_t,
                              typename BatchExec_worker<F>::output_t, F>;
template <class F>
using BatchExec = BatchExec_<BatchExec_worker<F>::N, typename BatchExec_worker<F>::input_t,
                             typename BatchExec_worker<F>::output_t, F>;
#endif // BATCHEXEC_H_
