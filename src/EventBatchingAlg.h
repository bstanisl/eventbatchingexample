#ifndef EVENTBATCHING_EVENTBATCHINGALG_H
#define EVENTBATCHING_EVENTBATCHINGALG_H 1

#include "BatchExec.h"
#include "EventBatching/Summary.h"
#include "Worker.h"

#include "AthenaBaseComps/AthReentrantAlgorithm.h"

#include "xAODJet/JetContainer.h"


class EventBatchingAlg : public AthReentrantAlgorithm {
  public:
    EventBatchingAlg(const std::string& name, ISvcLocator* pSvcLocator);
    virtual ~EventBatchingAlg();

    virtual StatusCode initialize() override; // once, before any input is loaded
    virtual StatusCode execute(const EventContext& ctx) const override; // per event
    virtual StatusCode finalize() override; // once, after all events processed

  private:
    SG::ReadHandleKey<xAOD::JetContainer> m_jetsKey{this, "JetsKey", "AnalysisJets",
                                                    "Jet container key"};
    SG::WriteHandleKey<Summary> m_summaryKey{this, "SummaryKey", "DemoSummary", "Summary key"};

    // Pointer so it can be created in initialize() and destroyed in finalize()
    EventBatchingDemoUtils::Worker* m_worker{nullptr};
    BatchExec<EventBatchingDemoUtils::Worker>* m_batch{nullptr};
};

#endif //> !EVENTBATCHING_EVENTBATCHINGALG_H
