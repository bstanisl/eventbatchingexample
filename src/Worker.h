#ifndef WORKER_H_
#define WORKER_H_

#include <algorithm>
#include <execution>

#include <TFile.h>
#include <TTree.h>

#include "EventBatching/Summary.h"

namespace EventBatchingDemoUtils {
constexpr int BatchSize = 8;
struct alignas(2 * 256 * alignof(float)) WkInput {
    float pt[256] = {0.f};
    float eta[256] = {0.f};
};

class Worker {
    using Input = WkInput;
    using Output = Summary;

    float pt_scratch[256] = {0.f};
    Output out_for_tuple{};
    TFile out_file;
    TTree* out_tree;

  public:
    Worker() : out_file("output.root", "RECREATE") {
        out_tree = new TTree("summary", "summary", 99, &out_file);
        out_tree->Branch("summary", &out_for_tuple, "nJet/I:pT1/F:pT3/F:eta_pT1/F");
        out_tree->Branch("nPt", &out_for_tuple.n_jet, "nPt/I");
        out_tree->Branch("pts", &pt_scratch, "pts[nPt]/F");
    }
    Worker(const Worker&) = delete;
    ~Worker() {
        out_file.Write();
        out_file.Close();
    }

    void operator()(int filled, const std::array<Input, BatchSize>& in,
                    std::array<Output, BatchSize>& out) {
        // Usually you would send the work off to a GPU or ONNXRuntime or something
        // For demonstration purposes, we just loop
        for (int i = 0; i < filled; ++i) {
            int max_idx = std::max_element(std::execution::par_unseq, std::begin(in[i].pt),
                                           std::end(in[i].pt))
                          - std::begin(in[i].pt);
            out[i].high_eta = in[i].eta[max_idx];
            out[i].n_jet = std::count_if(std::execution::par_unseq, std::begin(in[i].pt),
                                         std::end(in[i].pt), [](float x) { return x != 0; });
            std::memset(pt_scratch, 0, sizeof(pt_scratch));
            std::partial_sort_copy(std::execution::par_unseq, std::begin(in[i].pt), std::end(in[i].pt),
                                   std::begin(pt_scratch), std::end(pt_scratch),
                                   std::greater<float>{});
            out[i].high_pt = pt_scratch[0];
            out[i].mid3_pt = pt_scratch[2];

            // Fill tree -- something else batching helps with
            out_for_tuple = out[i];
            out_tree->Fill();
        }
    }
};
} // namespace EventBatchingDemoUtils

#endif // WORKER_H_
