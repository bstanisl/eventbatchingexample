// EventBatching includes
#include "EventBatchingAlg.h"

EventBatchingAlg::EventBatchingAlg(const std::string& name, ISvcLocator* pSvcLocator)
    : AthReentrantAlgorithm(name, pSvcLocator) {}

EventBatchingAlg::~EventBatchingAlg() {}

StatusCode EventBatchingAlg::initialize() {
    ATH_MSG_INFO("Initializing " << name() << "...");
    //
    // This is called once, before the start of the event loop
    // Retrieves of tools you have configured in the joboptions go here
    //

    ATH_CHECK(m_jetsKey.initialize());
    ATH_CHECK(m_summaryKey.initialize());
    m_worker = new EventBatchingDemoUtils::Worker();
    m_batch = new BatchExec<EventBatchingDemoUtils::Worker>{*m_worker};
    return StatusCode::SUCCESS;
}

StatusCode EventBatchingAlg::finalize() {
    ATH_MSG_INFO("Finalizing " << name() << "...");
    delete m_batch;
    delete m_worker;
    return StatusCode::SUCCESS;
}

StatusCode EventBatchingAlg::execute(const EventContext& ctx) const {
    ATH_MSG_DEBUG("Executing " << name() << "...");
    setFilterPassed(false, ctx); // optional: start with algorithm not passed

    SG::ReadHandle<xAOD::JetContainer> jets_h(m_jetsKey);
    SG::WriteHandle<Summary> summary_h(m_summaryKey);
    auto* jets = jets_h.get(ctx);
    int n_jets = jets->size();
    if (n_jets > 256) {
        // reject events with too many jets
        return StatusCode::SUCCESS;
    }
    // Get slot
    auto slot = m_batch->get_slot();
    // Load input
    auto& in = *slot.input;
    for (int idx = 0; auto&& jet : *jets) {
        in.pt[idx] = jet->pt();
        in.eta[idx] = jet->eta();
        ++idx;
    }
    // Wait for processing
    slot.wait_for_output();
    // Write output
    summary_h.put(std::make_unique<Summary>(*slot.output));

    setFilterPassed(true, ctx); // if got here, assume that means algorithm passed
    return StatusCode::SUCCESS;
}
