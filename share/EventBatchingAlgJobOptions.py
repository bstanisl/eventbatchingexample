import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AlgSequence import AlgSequence
from AthenaConfiguration.AllConfigFlags import ConfigFlags

from AthenaCommon.AlgScheduler import AlgScheduler
AlgScheduler.SchedulerSvc.PreemptiveBlockingTasks = True
AlgScheduler.SchedulerSvc.MaxBlockingAlgosInFlight = -1

svcMgr.EventSelector.InputCollections = [
    "/srv/EventBatching2/files/test_physlite.pool.root.1"
]

algSeq = AlgSequence()
algSeq += CfgMgr.EventBatchingAlg(
    "EventBatching", Blocking=False
)  # adds an instance of your alg to the main alg sequence
