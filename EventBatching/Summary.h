#ifndef SUMMARY_H_
#define SUMMARY_H_

struct alignas(16) Summary {
    int n_jet;
    float high_pt;
    float mid3_pt;
    float high_eta;
};

#include "xAODCore/CLASS_DEF.h"
CLASS_DEF(Summary, 2010000, 1);
#endif // SUMMARY_H_
